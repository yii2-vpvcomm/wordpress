<?php
/**
 * Created at: 31.03.2018 19:03
 * @author vpvcomm <vpvcomm@gmail.com>
 * @link http://vpvcomm.ru/
 * @copyright Copyright (c) 2018 vpvcomm
 */

namespace vpvcomm\wordpress\models;

use Yii;

/**
 * This is the model class for table "termmeta".
 *
 * @property string $meta_id
 * @property string $term_id
 * @property string $meta_key
 * @property string $meta_value
 */
class WpTermmeta extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%termmeta}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['term_id'], 'integer'],
            [['meta_value'], 'string'],
            [['meta_key'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'meta_id' => 'Meta ID',
            'term_id' => 'Term ID',
            'meta_key' => 'Meta Key',
            'meta_value' => 'Meta Value',
        ];
    }
}
