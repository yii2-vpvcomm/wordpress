<?php
/**
 * Created at: 31.03.2018 19:03
 * @author vpvcomm <vpvcomm@gmail.com>
 * @link http://vpvcomm.ru/
 * @copyright Copyright (c) 2018 vpvcomm
 */

namespace vpvcomm\wordpress\models;

use Yii;

/**
 * This is the model class for table "term_relationships".
 *
 * @property string $object_id
 * @property string $term_taxonomy_id
 * @property int $term_order
 */
class WpTermRelationships extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%term_relationships}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['object_id', 'term_taxonomy_id'], 'required'],
            [['object_id', 'term_taxonomy_id', 'term_order'], 'integer'],
            [['object_id', 'term_taxonomy_id'], 'unique', 'targetAttribute' => ['object_id', 'term_taxonomy_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'object_id' => 'Object ID',
            'term_taxonomy_id' => 'Term Taxonomy ID',
            'term_order' => 'Term Order',
        ];
    }
}
